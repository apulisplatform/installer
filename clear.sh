#!/bin/bash
install_dir=$(pwd)
sys_arch=$(uname -m)

echo -e "\n-------------------------------delete Apulis AI Platform----------------------------"
rm -rf  credentials

echo -e "\n-------------------------------uninstall Apulis AI Platform----------------------------"
bash applyrm.sh

rm -rf build
rm -rf app

echo -e "\n---------------------------------uninstall kubernetes----------------------------------"
kubeadm reset -f

rm -rf /etc/nginx
rm -rf /opt/kube
rm -rf /etc/kubernetes

systemctl stop kubelet

rm -rf $HOME/.kube
rm -rf /root/.kube
rm -rf /var/lib/etcd

echo -e "\n-----------------------------------uninstall docker------------------------------------"
systemctl disable docker.service && systemctl stop docker.service && systemctl daemon-reload

echo -e "\n-------------------------------------delete docker file----------------------------------------"

docker_arr=(
  "containerd"
  "containerd-shim"
  "ctr"
  "docker"
  "dockerd"
  "docker-init"
  "docker-proxy"
  "runc"

)

if [[ -f "/usr/local/bin/docker" ]]
  then
    
    for item in ${docker_arr[*]}
    do
        rm -rf /usr/local/bin/$item
    done

fi

rm -rf /etc/docker
rm -rf $HOME/.docker
rm -rf /etc/systemd/system/docker.service


echo -e "\n-------------------------------------delete user: dlwsadmin----------------------------------------"
grep dlwsadmin /etc/passwd
if [ $? = 0 ]
then
    sed -i '/dlwsadmin/'d $HOME/.ssh/authorized_keys
    userdel -r dlwsadmin
    rm -rf /home/dlwsadmin
else
    echo "There is no user named dlwsadmin"
fi

if [ $? = 0 ]
then
    echo -e "\n#>>>>>>-satus: The installation files have been cleared "
else
    echo -e "\n#>>>>>>-satus: The installation files cleared  fail ,pls manual clear"
fi