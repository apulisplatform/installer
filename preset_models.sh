#!/bin/bash
#status=1

#function用法:检查数据库是否创建,并导入数据
#$1:导入的功能modules-name  $2:导入的database-name  $3:导入的sql-file

importdatabase_function(){
for((i=1;i<=3;i++));
do
        echo -e "\nPolling 3 times, The $i time check that the "$2" database was created successfully,timeout 5 seconds"

        sleep 5

	postgre_name=$(kubectl get pod -A | grep postgres | awk -F " +" '{print $2}')
        
        kubectl exec -i -n kube-system $postgre_name -- psql -U postgres \
        -c "SELECT u.datname FROM pg_catalog.pg_database u where u.datname='$2';" | grep -w "$2"

        if [[ $? = 0 ]];then

                echo -e "\033[32m>>>#----------$2 database create successfully,start import data----------\033[0m"

                dockerid=$(docker ps | grep 'k8s_postgres_postgres' | awk '{print $1 }' | awk 'NR=1' | head -1)

                docker cp ./$3 $dockerid:/home/

                docker exec -i $dockerid /bin/bash -c "psql -d $2 -U postgres -f /home/$3" > /dev/null 2>&1


                if [ $? -eq 0 ];then
                       echo -e "\033[32m>>>#----------$1 table data Imported succeed!----------\033[0m"
                else
                       echo -e "\033[31m>>>#----------$1 table data Imported failed!----------\033[0m"
                fi
                break
        else
                echo -e "\033[31m>>>#----------$2 database create failed----------\033[0m"
        fi
done
}

set -e
importdatabase_function models ai_arts preset_models.sql

#Initialize the number of "VC" devices
#postgre_name=$(kubectl get pod -A | grep postgres | awk -F " +" '{print $2}')
compute_core=$(nvidia-smi -L | wc -l)
dlwdb_name=$(kubectl exec -i -n kube-system $postgre_name -- psql -U postgres -c "\l" | grep DLW | awk -F " +" '{print $2}')

#kubectl exec -i -n kube-system $postgre_name -- psql  -d DLWSCluster-02c9e1f5-8ec2-4d09-850f-5a0c99415e3c -U postgres -c "select * from vc;"
kubectl exec -i -n kube-system $postgre_name -- psql  -d $dlwdb_name -U postgres -c "update vc set quota = '{\"nvidia_gpu_amd64\": $compute_core}' where id =1;"
kubectl exec -i -n kube-system $postgre_name -- psql  -d $dlwdb_name -U postgres -c "update vc set metadata = '{\"nvidia_gpu_amd64\":{\"user_quota\":$compute_core}}' where id =1;"
