#!/bin/bash

eth_ip=$1
exeth_ip=$2
input_domain=$3
harbor_host="harbor.apulis.cn:8443"
project="aiarts_v1.6.0_rc4_single"
version="v1.6.0"
component=""

echo -e "\n\033[34m#>>>>>>-Downloading component images-------------------------------------\033[0m"

image=(
        "apulistech/nginx:v1.6.0"
        "apulistech/nginx_amd64:v1.6.0"
        "apulistech/user_fronted:v1.6.0-rc5"
        "apulistech/user_backend:v1.6.0-rc5"
        "apulistech/aiarts-frontend:v1.6.0-rc5-hotfix1"
        "apulistech/cvat-frontend:v0.2.0"
        "apulistech/cvat-backend:v0.2.0"
        "apulistech/dlworkspace-webui3:v1.6.0-rc5"
        "apulistech/dlworkspace-restfulapi2:v1.6.0-rc5"
        "apulistech/aiarts-backend:v1.6.0-rc5"
        "apulistech/job-exporter:v1.5.0"
        "apulistech/dlworkspace_openresty:v1.5.0"
        "ascendhub/npu-exporter:v20.2.0"
        "apulistech/mlflow:v1.0.0"
        "postgres:11.10-alpine"
        "vc-webhook-manager:v0.0.1"
        "vc-scheduler:v0.0.1"
        "vc-controller-manager:v0.0.1"
        "apulistech/watchdog:1.9"
        "apulistech/grafana-zh:6.7.4"
        "apulistech/knative-serving-webhook:latest"
        "apulistech/knative-serving-controller:latest"
        "apulistech/knative-serving-autoscaler:latest"
        "apulistech/knative-serving-activator:latest"
        "apulistech/knative-net-istio-webhook:latest"
        "apulistech/knative-net-istio-controller:latest"
        "apulistech/istio-proxy:latest"
        "apulistech/istio-pilot:latest"
        "apulistech/grafana:6.7.4"
        "bash:5"
        "apulistech/dlworkspace_gpu-reporter:latest"
        "prom/prometheus:v2.18.0"
        "directxman12/k8s-prometheus-adapter:v0.7.0"
        "jessestuart/prometheus-operator:v0.38.0"
        "prom/alertmanager:v0.20.0"
        "redis:5.0.6-alpine"
        "prom/node-exporter:v0.18.1"
        "nvidia/k8s-device-plugin:1.11"
        "busybox:1.28"
        "apulistech/kfserving-kube-rbac-proxy:latest"
        "apulistech/kfserving-manager:latest"
)

for im in ${image[*]}
do
  docker pull $harbor_host/$project/$im 2>&1
  if [ $? -ne 0 ];then
    for((i=1;i<=3;i++))
        do
        if [ $i -eq 3 ];then
           echo -e "\033[31m $harbor_host/$project/$im PULL$i次失败,请确认网络是否异常033[31m"
           exit 1
        else
            docker pull $harbor_host/$project/$im
            echo -e "\033[34m $harbor_host/$project/$im PULL失败重试第$i次033[31m"
        fi
        done
  fi
done

base_dir=`pwd`

arr1=(
    "storage-nfs"
    "nvidia-device-plugin"
    "postgres"
    "restfulapi2"
    "custom-user-dashboard"
    "jobmanager2"
    "nginx"
    "openresty"
    "webui3"
    "aiarts-backend"
    "aiarts-frontend"
#    "mlflow"
    "volcanosh"
#    "image-label"
#    "data-platform"
    "model-manager"
)

#修改环境ip
echo -e "\n\033[34m#>>>>>>-Update IP for service yaml -----------------------------------------\033[0m"

for item1 in ${arr1[*]}
do
        n=`cd $base_dir/build/$item1 && ls -l | grep ^- | awk -F " +" '{print $9}'`
        cd $base_dir/build/$item1 && for file in $n ; do ( sed -i s/conip/192.168.2.192/g $file); done ; cd $base_dir

done

#检查kube-dns and configure nginx.conf to openresty service
echo -e "\n\033[34m#>>>>>>-Check kube-dns and configure nginx.conf to openresty service-------------------\033[0m"
now_dnssvcip=`kubectl get svc -n kube-system kube-dns | grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'`
sed -i "s/dnsip/${now_dnssvcip}/g" /etc/nginx/nginx.conf

#部署应用前检查nfs存储状态
echo -e "\n\033[34m#>>>>>>-Check NFS Service before Running Apulis-----------------------\033[0m"
stat=`systemctl status rpcbind | grep Active | awk -F " +" '{print $3}'`

if [[ $stat = "active" ]];then
        echo -e "nfs is installed"
else
        bash nfs.sh
fi

#启动上层pod服务
echo -e "\n\033[34m#>>>>>>-Startup services -------------------------------\033[0m"

for item1 in ${arr1[*]}
do
	n=`cd $base_dir/build/$item1 && ls | grep '^[0-9]'`
	cd $base_dir/build/$item1 \
        && for file in $n
        do 
            echo -e "\n\033[33m#>>>>>>-$base_dir/build/$item1/${file}: \033[0m"; kubectl apply -f $file && sleep 1
        done
        cd $base_dir		
done

sleep 3

echo -e "\n\033[34m#>>>>>>-Startup istio pre-render -----------------------------------------------\033[0m"

cd $base_dir/build/istio && bash pre-render.sh && cd $base_dir

echo -e "\n\033[34m#>>>>>>-Startup knative and cvat -----------------------------------------\033[0m"
arr2=(
    "knative"
#    "kfserving"
    "cvat"
    "cvat-model"
)

for item2 in ${arr2[*]}
do
	n=`cd $base_dir/build/$item2 && ls -l | grep ^- | awk -F " +" '{print $9}'`
        cd $base_dir/build/$item2 && for file in $n ; do ( sed -i s/conip/192.168.2.192/g $file); done ; cd $base_dir
	n=`cd $base_dir/build/$item2 && ls | grep '^[0-9]'`
	cd $base_dir/build/$item2 \
        && for file in $n
        do 
            echo -e "\n\033[33m#>>>>>>-$base_dir/build/$item2/${file}: \033[0m"; kubectl apply -f $file && sleep 1
        done
        cd $base_dir		
done

sleep 3

#部署prometheus monitor
echo -e "\n\033[34m#>>>>>>-Update prometheus monitor ----------------------------------------\033[0m"
sed -i s/conip/192.168.2.192/g $base_dir/build/monitor/03.watchdog.yaml

#部署custommetrics
echo -e "\n\033[34m#>>>>>>-Startup custommetrics service ------------------------------------\033[0m"

base_dir=`pwd`

custom=(
    "01.prometheus_operator.yaml"
    "02.prometheus_instance.yaml"
    "03.custom_metrics.yaml"
    "sample-metrics-app.yaml"
)

for customs in ${custom[*]}
do
    echo -e "\n\033[33m#>>>>>>-$base_dir/build/custommetrics/${customs} first running status: \033[0m" && kubectl apply -f $base_dir/build/custommetrics/$customs && sleep 3
done

for customs in ${custom[*]}
do
    echo -e "\n\033[33m#>>>>>>-$base_dir/build/custommetrics/${customs} second running status: \033[0m" && kubectl apply -f $base_dir/build/custommetrics/$customs && sleep 3
done

#部署prometheus grafana监控
echo -e "\n\033[34m#>>>>>>-Startup prometheus service ---------------------------------------\033[0m"
prom=(
    "01.node-exporter.yaml"
    "02.job-exporter.yaml"
    "03.watchdog.yaml"
    "04.prometheus-alerting.yaml"
    "05.prometheus.yaml"
    "06.grafana-ini.yaml"
    "07.grafana-config.yaml"
    "07.grafana-zh-config.yaml"
    "08.grafana.yaml"
    "08.grafana-zh.yaml"
    "09.alert-templates.yaml"
    "10.npu-exporter.yaml"
    #"alert-manager.yaml"
)

for proms in ${prom[*]}
do
    echo -e "\n\033[33m#>>>>>>-$base_dir/build/monitor/$proms/${proms} running status: \033[0m" && kubectl apply -f $base_dir/build/monitor/$proms && sleep 3
done

sleep 5

#执行完apply.sh配置菜单
echo -e "\n\033[34m#>>>>>>-Configure menus data ----------------------------------------------\033[0m"
echo -e "Import menus_data"
bash preset_menus.sh

#返回平台部署状态
if [[ $? -ne 0 ]];then
    echo -e "\n\033[31m#>>>>>>-Apulis AI Platform Install Failed ! \033[0m"
else
    echo -e "\n\033[32m#>>>>>>-Apulis AI Platform Install Succeed ! \033[0m"
    kubectl get node
fi
