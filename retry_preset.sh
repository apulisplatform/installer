#!/bin/bash
# 出现网络超时再次拉取和配置资源

check_network(){
  if ping -q -c 4 -W 1 baidu.com >/dev/null; then
    echo -e "\033[34m#>>>>>>-The network is up ! \033[0m"
  else
    echo -e "\033[31m#>>>>>>-The network is down, please check the network ! \033[0m"
    exit
  fi
}

#安装后, 预置数据
config_function(){
        datasets=(cifar-100-python fashion-mnist)

        if [ ! -d "/data/nfs/pvc/aiplatform-model-data/dataset/storage/fashion-mnist"]; then
          # Download fashion-mnist
          sudo git clone https://github.com/zalandoresearch/fashion-mnist.git
          sudo mv $install_dir/fashion-mnist /data/nfs/pvc/aiplatform-model-data/dataset/storage/
        fi
        if [ ! -d "/data/nfs/pvc/aiplatform-model-data/dataset/storage/cifar-100-python"]; then
          # Download cifar-100
          sudo wget http://www.cs.toronto.edu/~kriz/cifar-100-python.tar.gz
          sudo mv $install_dir/cifar-100-python.tar.gz /data/nfs/pvc/aiplatform-model-data/dataset/storage/  \
             && cd /data/nfs/pvc/aiplatform-model-data/dataset/storage/   \
             && tar -zxf cifar-100-python.tar.gz   \
             && cd $install_dir
        fi

        # Download engin images
        echo -e "\n\033[34m#>>>>>>-Download preset images--------------------------------------------\033[0m"
        image=(
            "harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/tensorflow:2.3.0-gpu"
            "harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/pytorch:1.6.0-gpu"
            "harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/mxnet:2.0.0-gpu-py3"
            "harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/mindspore:1.1.1-gpu"
	          "harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/tensorflow:1.15.0-gpu-py3"
            "harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/tensorflow:1.14.0-gpu-py3"
        )

        for im in ${image[*]}
        do
          sudo docker pull $im 2>&1
          if [ $? -ne 0 ];then
            for((i=1;i<=3;i++))
                do
                if [ $i -eq 3 ];then
                   echo -e "\033[31m $im PULL$i次失败,请确认网络是否异常033[31m"
                   exit 1
                else
                    sudo docker pull $im
                    echo -e "\033[34m $im PULL失败重试第$i次033[31m"
                fi
                done
          fi
        done    

}


#########################################################################################################
##主入口

echo """
It will re-try to download or pull remote resources for setup platform, Please be patient...
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
"""

install_dir=$(pwd)

# Check network
check_network

# Re-apply,sh
bash apply.sh
config_function


