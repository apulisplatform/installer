#!/bin/bash

# create platform user dlwsadmin
echo -e "\n\033[34m#>>>>>>-configure platform admin--------------------------------------\033[0m"

userdlws(){
        user_name=$(echo $HOME | awk -F'/' '{print $NF}')
        sed -i "s/#   StrictHostKeyChecking ask/    StrictHostKeyChecking no/g" /etc/ssh/ssh_config
        rm -rf /home/dlwsadmin/.ssh/{known_hosts,id_rsa*}
        
        echo -e "#>>>>>>-create id_rsa for dlwsadmin"
        su dlwsadmin -s /bin/bash -c 'ssh-keygen -q -P "" -f /home/dlwsadmin/.ssh/id_rsa > /dev/null'
        
        if [[ ! -d "$HOME/.ssh" ]];then
            echo -e "#>>>>>>-create .ssh dir in $HOME"
            su -s /bin/bash -c "mkdir $HOME/.ssh && chown -R $user_name.$user_name $HOME/.ssh"
        fi
        echo -e "#>>>>>>-copy dlwsadmin id_rsa.pub to $user_name in $HOME/.ssh"
	    sudo -s /bin/bash -c "cat /home/dlwsadmin/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys"

        echo -e "#>>>>>>-chown $user_name.$user_name $HOME/.ssh/authorized_keys"
	    chown -R $user_name.$user_name $HOME/.ssh/authorized_keys
}

grep dlwsadmin /etc/passwd
if [[ $? = 1 ]]
then
    echo -e "#>>>>>>-create platform user:dlwsadmin"
    sudo useradd -d /home/dlwsadmin -m dlwsadmin && sudo chown -R dlwsadmin.dlwsadmin /home/dlwsadmin
    userdlws
else
    userdlws
fi