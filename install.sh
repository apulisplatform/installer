#!/bin/bash


#判断ip输入是否正确函数
input_ip_function(){
    while :
    do
        read -p "Please enter IP address for the platform LAN service: " eth_ip
        #判断输入的ip地址是否仅由数字和点组成，并将值赋给m。
        #判断输入的ip地址的小数点的个数，复制给n
        m=`echo $eth_ip |sed 's/[0-9.]//g'`
        n=`echo $eth_ip |sed 's/[0-9]//g'|wc -c`
        #截取ip地址的四个数字的部分，并分别复制。
        n1=`echo $eth_ip |cut -d'.' -f1`
        n2=`echo $eth_ip |cut -d'.' -f2`
        n3=`echo $eth_ip |cut -d'.' -f3`
        n4=`echo $eth_ip |cut -d'.' -f4`
        #第一个if通过m和n来判断输入的ip是否符合要求
        if [ -z $m ] && [ $n -eq 4 ] && [ -n $n1 ] && [ -n $n2 ] && [ -n $n3 ] && [ -n $n4 ];then
        #第二个if通过n1到n4来判断输入的范围是否符合要求。
        if [ $n1 -ge 0 ] && [ $n1 -le 255 ] && [ $n2 -ge 0 ] && [ $n2 -le 255 ] && [ $n3 -ge 0 ] && [ $n3 -le 255 ] && [ $n4 -ge 0 ] && [ $n4 -le 255 ]
        then
            echo -e "\nThe service IP you entered is：$eth0_ip"
            break
        fi
        else
          echo -e "\033[31mThe IP test error, please re-enter !\033[0m"
        fi
    done    
}

#判断外网ip输入是否正确函数
input_exip_function(){
    while :
    do
        read -p "Please enter the IP for public network access. If you don't have public IP, please use LAN IP instead: " exeth_ip
        #判断输入的ip地址是否仅由数字和点组成，并将值赋给m。
        #判断输入的ip地址的小数点的个数，复制给n
        m=`echo $exeth_ip |sed 's/[0-9.]//g'`
        n=`echo $exeth_ip |sed 's/[0-9]//g'|wc -c`
        #截取ip地址的四个数字的部分，并分别复制。
        n1=`echo $exeth_ip |cut -d'.' -f1`
        n2=`echo $exeth_ip |cut -d'.' -f2`
        n3=`echo $exeth_ip |cut -d'.' -f3`
        n4=`echo $exeth_ip |cut -d'.' -f4`
        #第一个if通过m和n来判断输入的ip是否符合要求
        if [ -z $m ] && [ $n -eq 4 ] && [ -n $n1 ] && [ -n $n2 ] && [ -n $n3 ] && [ -n $n4 ];then
        #第二个if通过n1到n4来判断输入的范围是否符合要求。
        if [ $n1 -ge 0 ] && [ $n1 -le 255 ] && [ $n2 -ge 0 ] && [ $n2 -le 255 ] && [ $n3 -ge 0 ] && [ $n3 -le 255 ] && [ $n4 -ge 0 ] && [ $n4 -le 255 ]
        then
            echo -e "\nThe public IP you entered is：$exeth_ip"
            #用户不配置域名，默认/etc/hostname-fqdn内容为"hostname-fqdn"
            echo "hostname-fqdn" > /etc/hostname-fqdn
            break
        fi
        else
          echo -e "\033[31mThe public IP test error, please re-enter !\033[0m"
        fi
    done    
}

#判断域名输入是否正确函数
yuming_function(){
        domain_flag=''
        until [ "$domain_flag" != '' ]
        do
            read -p "Please enter domainname: " input_domain
            domain_flag=$(echo $input_domain | gawk '/^(http(s)?:\/\/)?[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+(:[0-9]{1,5})?$/{print $0}')
            if [ ! -n "${domain_flag}" ]; then
                echo -e "\033[31mDomainname test error, please re-enter !\033[0m"
            fi
        done
        echo -e "The domainname you entered is：$input_domain"
        #将用户定义的域名配置到/etc/hostname-fqdn，nginx服务起来后会通过hostpatch挂载读取内容
        #访问形式为https://用户定义的域名
        echo "$input_domain" > /etc/hostname-fqdn
}

off_fw_swap_function(){
        #关闭防火强墙################################
        echo -e "\n\033[34m#>>>>>>-Stop firewalld------------------------------------------------\033[0m"
        check_fw=`cat /etc/systemd/system/firewalld.service | wc -l`
        if [ $check_fw -ne 0 ];then
           systemctl stop firewalld
           systemctl disable firewalld
           echo -e "\033[32m Please firewalld off \033[0m"
        else
           echo -e "\033[32m Firewalld off \033[0m"
        fi

        echo -e "\n\033[34m#>>>>>>-Swapoff-------------------------------------------------------\033[0m"
        swapoff -a && sysctl -w vm.swappiness=0
        sed -ri 's/.*swap.*/#&/' /etc/fstab

}

#安装函数
install_function(){
        #修改配置IP
        echo -e "\n\033[34m#>>>>>>-Modify the IP address of the apply.sh-------------------------\033[0m"
        
        oldip=`grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' apply.sh | head -n1`
        sed -i s/$oldip/$eth_ip/g apply.sh 

        #下载二进制文件
        echo -e "\n\033[34m#>>>>>>-Prepare file--------------------------------------------------\033[0m"
        
        prepare_file=(
            "app_$sys_arch.tar.gz"
            "mnist.tar.gz"
            "model-gallery.tar.gz"
			"dog-vs-cat.tar.gz"
        )

        for download_files in ${prepare_file[*]}
        do
            if [[ ! -f "$install_dir/$download_files" ]]
            then
                wget http://download.apulis.cn:8085/downloads/$download_files
                tar zxf $download_files
            fi
        done

        #拷贝服务yaml模板到build目录,启动服务用build目录里的yaml文件
        echo -e "\n\033[34m#>>>>>>-Copy service yaml templates to build---------------------------\033[0m"
        if [[ ! -d "build" ]]
        then
            cp -r yaml build
        fi
        
        #configure restful and webui service access Ip Address
        bash ingress.sh

        #关闭防火强墙
        off_fw_swap_function

        #部署docker
        echo -e "\n\033[34m#>>>>>>-Check docker status-------------------------------------------\033[0m"

        stat=`systemctl status docker | grep Active | awk -F " +" '{print $3}'`

        if [[ $stat = "active" ]];then
                echo -e "\033[33mDocker is installed\033[0m"
        else
                echo -e "\033[33mStart install docker\033[0m"
                bash docker.sh
        fi

        sleep 3

        #部署kubernetes
        echo -e "\n\033[34m#>>>>>>-Check kubernetes status---------------------------------------\033[0m"
        
        stat=`systemctl status kubelet | grep Active | awk -F " +" '{print $3}'`
        
        if [[ $stat = "active" ]];then
            echo -e "\033[33mKubernetes is installed"
        else
            echo -e "\033[33mkubernetes no install , start install kubernetes"
                bash k8s.sh $eth_ip
        fi

        sleep 10

        #部署nfs存储
        echo -e "\n\033[34m#>>>>>>-Check nfs status----------------------------------------------\033[0m"
        
        stat=`systemctl status rpcbind | grep Active | awk -F " +" '{print $3}'`

        if [[ $stat = "active" ]];then
                echo -e "\033[33mnfs is installed\033[0m"
        else
                echo -e "\033[33mstart install nfs\033[0m"
                bash nfs.sh
                showmount -e $eth_ip
                mkdir /mntdlws
                mount $eth_ip:/data/nfs/pvc/ /mntdlws/
        fi

        #生成Apulis AI Platform password
        echo -e "\n\033[34m#>>>>>>-Configure Apulis AI Platform Credentials------------------------------------\033[0m"
        if [[ ! -d "credentials" ]]
        then
            mkdir credentials

            #mysql数据库密码
            echo -e "#>>>>>>-create mysql password"
            mysql_pwd=`< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c${1:-10};echo;` && echo $mysql_pwd >> credentials/mysql.pwd

            #admin管理员密码
            echo -e "#>>>>>>-create admin password"
            admin_pwd=`< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c${1:-10};echo;` && echo $admin_pwd >> credentials/admin.pwd
            
            #postgres数据密码
            echo -e "#>>>>>>-create postgres password"
            postgres_pwd=`< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c${1:-10};echo;` && echo $postgres_pwd >> credentials/postgres.pwd

            #harbor管理员密码
            echo -e "#>>>>>>-create harbor password"
            harbor_pwd=`< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c${1:-10};echo;` && echo $harbor_pwd >> credentials/harbor.pwd

            #harbor数据库密码
            echo -e "#>>>>>>-create barbor database password"
            harbordb_pwd=`< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c${1:-10};echo;` && echo $harbordb_pwd >> credentials/harbor-db.pwd

            #cluster id创建
            echo -e "#>>>>>>-create cluster id"
            cluster_id=$(uuidgen) && echo $cluster_id >> credentials/cluster-id
        fi

        
        #配置Apulis AI Platform password >>> yaml file
        #mysql数据库密码
        echo -e "#>>>>>>-Apply mysql password"
        mysql_pwdvule=$(cat credentials/mysql.pwd)
        sed -i "s/mysql_old_pwd/$mysql_pwdvule/g" $install_dir/build/mysql/01.mysql.yaml

        #admin管理员密码
        echo -e "#>>>>>>-Apply admin password"
        admin_pwdvule=$(cat credentials/admin.pwd)
        sed -i "s/admin_old_pwd/$admin_pwdvule/g" $install_dir/build/custom-user-dashboard/local.config
        sed -i "s/admin_old_pwd/$admin_pwdvule/g" $install_dir/build/custom-user-dashboard/01.custom-user-dashboard-cm.yaml

        #postgres数据密码
        echo -e "#>>>>>>-Apply postgres password"
        postgres_pwdvule=$(cat credentials/postgres.pwd)
        sed -i "s/postgres_old_pwd/$postgres_pwdvule/g" $install_dir/build/aiarts-backend/01.aiarts_cm.yaml
        sed -i "s/postgres_old_pwd/$postgres_pwdvule/g" $install_dir/build/custom-user-dashboard/local.config
        sed -i "s/postgres_old_pwd/$postgres_pwdvule/g" $install_dir/build/custom-user-dashboard/01.custom-user-dashboard-cm.yaml
        sed -i "s/postgres_old_pwd/$postgres_pwdvule/g" $install_dir/build/mlflow/01.mlflow.yaml
        sed -i "s/postgres_old_pwd/$postgres_pwdvule/g" $install_dir/build/model-manager/01.models_cm.yaml
        sed -i "s/postgres_old_pwd/$postgres_pwdvule/g" $install_dir/build/postgres/01.postgres.yaml
        sed -i "s/postgres_old_pwd/$postgres_pwdvule/g" $install_dir/build/restfulapi2/01.restful-cm.yaml
        sed -i "s/postgres_old_pwd/$postgres_pwdvule/g" $install_dir/build/restfulapi2/config.yaml

        #cluster id创建
        echo -e "#>>>>>>-Apply cluster id"
        cluster_idvule=$(cat credentials/cluster-id)
        sed -i "s/cluster_old_id/$cluster_idvule/g" $install_dir/build/restfulapi2/01.restful-cm.yaml
        sed -i "s/cluster_old_id/$cluster_idvule/g" $install_dir/build//restfulapi2/config.yaml
        sed -i "s/cluster_old_id/$cluster_idvule/g" $install_dir/build/webui3/userconfig.json
        sed -i "s/cluster_old_id/$cluster_idvule/g" $install_dir/build/webui3/01.webui-cm.yaml

        #部署Apulis AI Platform
        echo -e "\n\033[34m#>>>>>>-Install Apulis AI Platform------------------------------------\033[0m"

        #检查nginx配置
        echo -e "\n\033[34m#>>>>>>-Configure nginx service---------------------------------------\033[0m"
        if [[ ! -d "/etc/nginx/ssl" ]];then
            mkdir -p /etc/nginx/conf.other
            mv app/ssl /etc/nginx/ && mv app/default.conf /etc/nginx/conf.other/ && mv app/nginx.conf /etc/nginx/
        fi

        #配置istioctl
        echo -e "\n\033[34m#>>>>>>-Configure istioctl command tool-------------------------------\033[0m"
        if [[ ! -f "/opt/kube/bin/istioctl" ]];then
            mv app/istioctl /opt/kube/bin/ && chmod +x /opt/kube/bin/istioctl
        fi

        #配置calico
        echo -e "\n\033[34m#>>>>>>-Check calico network------------------------------------------\033[0m"
        
        stat=`kubectl get pod -n kube-system | grep calico | wc -l`

        if [[ $stat = 2 ]];then
                echo -e "\n\033[34m#>>>>>>-Calico network is ready,running apply.sh--------------\033[0m"
                #calico网络配置ok,开始执行apply.sh部署上层平台服务
                bash apply.sh
        else
                #calico网络没有running,则给出提示
                echo "\033[31mCalico network is no ready\033[0m"
        fi
}

#安装后, 预置数据
config_function(){
        # Deploy preset models sqls
        echo -e "\n\033[34m#>>>>>>-Deploy preset models --------------------------------------------\033[0m"
        postgres_stat=`kubectl get pod -A | grep postgres | awk -F " +" '{print $4}'`
        postgres_id=`kubectl get pod -A | grep postgres | awk -F " +" '{print $2}'`
        postgres_warn=`kubectl  describe  pod $postgres_id -n kube-system | grep  Events -A 5`
        if [ $postgres_stat == "Running"  ];then
          bash preset_models.sh
        else
          echo -e "\033[31m $postgres_warn\033[31m"
          echo -e "\033[31m Please check it \033[31m"
          bash clear.sh
          exit 1
        fi
        
        # Move models to nfs
        mv $install_dir/model-gallery /data/nfs/pvc/aiplatform-model-data/
        
        echo -e "\n\033[34m#>>>>>>-Import preset datas--------------------------------\033[0m"
        # Move dataset to nfs
        mkdir -pv /data/nfs/pvc/aiplatform-model-data/dataset/storage/
        chmod -R 777 /data/nfs/pvc/aiplatform-model-data/
        # Get datasets
        cd $install_dir
        mv $install_dir/dog-vs-cat.tar.gz /data/nfs/pvc/aiplatform-model-data/dataset/storage/
        mv $install_dir/mnist /data/nfs/pvc/aiplatform-model-data/dataset/storage/

        # Download cifar-100
        wget http://www.cs.toronto.edu/~kriz/cifar-100-python.tar.gz
        mv $install_dir/cifar-100-python.tar.gz /data/nfs/pvc/aiplatform-model-data/dataset/storage/  \
           && cd /data/nfs/pvc/aiplatform-model-data/dataset/storage/   \
           && tar -zxf cifar-100-python.tar.gz   \
           && cd $install_dir
        # Download fashion-mnist
        git clone https://github.com/zalandoresearch/fashion-mnist.git
        mv $install_dir/fashion-mnist /data/nfs/pvc/aiplatform-model-data/dataset/storage/


        # Download engin images
        echo -e "\n\033[34m#>>>>>>-Download preset images--------------------------------------------\033[0m"
        image=(
            "harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/tensorflow:2.3.0-gpu"
            "harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/pytorch:1.6.0-gpu"
            "harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/mxnet:2.0.0-gpu-py3"
            "harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/mindspore:1.1.1-gpu"
	        "harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/tensorflow:1.15.0-gpu-py3"
            "harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/tensorflow:1.14.0-gpu-py3"
        )

        for im in ${image[*]}
        do
          docker pull $im 2>&1
          if [ $? -ne 0 ];then
            for((i=1;i<=3;i++))
                do
                if [ $i -eq 3 ];then
                   echo -e "\033[31m $im PULL$i次失败,请确认网络是否异常033[31m"
                   exit 1
                else
                    docker pull $im
                    echo -e "\033[34m $im PULL失败重试第$i次033[31m"
                fi
                done
          fi
        done       
}

startup_installation(){
    #>>>>>>>>>>>>>>>>>>>>>>>>>>>>创建user:dlwsadmin
    bash create_user.sh
    #>>>>>>>>>>>>>>>>>>>>>>>>>>>>开始安装平台
    install_function
}

display_account(){
    # 上层平台服务部署完成后输出平台登录信息
    adpd=`cat credentials/admin.pwd`
    echo -e """
    #>>>>>>-Platform login information
    \033[32mAdminName: admin   
    Password: ${adpd}
    Platform Data Direction: /data/nfs/pvc\033[0m
    """
    echo -e ">>>>>>" $(date) 
    echo -e "\033[34m------------------------------- The Preset models has been successfully deployed ! ---------------------------- \033[0m"
}

#########################################################################################################
##主入口

#检测环境
bash init_env.sh
bash check.sh

install_dir=$(pwd)

## 获取系统架构:aarch64 or x86_64
sys_arch=$(uname -m)
export sys_arch=$sys_arch

# Show local main network
eth0_ip=$(ip addr show dev eth0 | grep "inet" | awk 'NR==1{print $2}' | cut -d'/' -f 1)
SUBNET=$(ip route | grep "src $MAINIP" | awk '{print $1}')
GATEWAYIP=$(ip route show | grep default | awk '{print $3}')
echo """
Local Ethernet:
Eth0 IP: ${eth0_ip}
Subnet:  ${SUBNET}
Gateway: ${GATEWAYIP}
"""
# Personal setup
eth_ip=""
exeth_ip=""
input_domain=""
while true
do
        # Get right local LAN IP
        input_ip_function
        export eth_ip=$eth_ip
        read -r -p "Do you neet to login platform through the internet ? [yes/no] " input_publicip
        read -r -p "Do you want login platform with domain(URL) ? [yes/no] " input_domainame

        if [[ "$input_publicip" =~ ^[Yy][Ee][Ss]$ ]] && [[ "$input_domainame" =~ ^[Yy][Ee][Ss]$ ]];then
            # 确认公网IP
            input_exip_function  
            export exeth_ip=$exeth_ip
            # 确认公网域名
            yuming_function 
            export input_domain=$input_domain
            #设置一个开关来判断平台访问的方式,1为有public和domainame
            export public_stat=1
            #将domainame传递给ingress.sh
            export input_domain=$input_domain

            startup_installation
            echo -e "\n\033[33m#>>>>>>-You have setup the public IP and domainname, you can access the platform in the following way !033[0m"
            echo -e "\033[32m#>>>>>>-Public IP: $exeth_ip   URL: $input_domain  \033[0m"
            display_account
            break
        elif [[ "$input_publicip" =~ ^[Yy][Ee][Ss]$ ]] && [[ "$input_domainame" =~ ^[Nn][Oo]$ ]];then
            # echo -e "\n\033[33m>#You deploy pattern：public network,no domainame\033[0m"
            input_exip_function 
            export exeth_ip=$exeth_ip
            #设置一个开关来判断平台访问的方式,2为有public-ip,无domainame
            export public_stat=2
            export exeth_ip=$exeth_ip

            startup_installation
            echo -e "\n\033[33m#>>>>>>-You only have setup public IP, you can access the platform in the following way !\033[0m"
            echo -e "\033[32m#>>>>>>-Public IP: $exeth_ip  \033[0m"
            display_account
            break
        elif [[ "$input_publicip" =~ ^[Nn][Oo]$ ]] && [[ "$input_domainame" =~ ^[Yy][Ee][Ss]$ ]];then
            yuming_function  
            export input_domain=$input_domain
            #设置一个开关来判断平台访问的方式,3为无public-ip,但有domainame
            export public_stat=3
            export input_domain=$input_domain

            startup_installation
            echo -e "\n\033[33m#>>>>>>-You only have setup domainname, you can access the platform in the following way !\033[0m"
            echo -e "\033[32m#>>>>>>-URL: $input_domain \033[0m"
            display_account
            break
        elif [[ "$input_publicip" =~ ^[Nn][Oo]$ ]] && [[ "$input_domainame" =~ ^[Nn][Oo]$ ]];then
            #用户不配置域名，默认/etc/hostname-fqdn内容为"hostname-fqdn"
            #访问形式为http://用户定义的ip
            echo "hostname-fqdn" > /etc/hostname-fqdn
            #设置一个开关来判断平台访问的方式, 4:无public-ip,无domainame
            export public_stat=4

            startup_installation
            echo -e "\n\033[33m#>>>>>>-You only have setup LAN, you can access the platform in the following way !\033[0m"
            echo -e "\033[32m#>>>>>>-LAN IP: ${eth_ip} \033[0m"
            display_account
            break    
        else
            echo "In order to access the platform easily, you must configure LAN IP!"
        fi
done

#>>>>>>>>>>>>>>>>>>>>>>>>>>>>配置预置镜像及数据集
config_function
