# Preset datas info

## Preset models templates

*Rerfer /data/model-gallery/models*

|model templates                     |datasets      |engin image                 |
|------------------------------------|--------------|----------------------------|
|ResNet50_mindspore_gpu              | dog-vs-cat   | mindspore:1.1.1-gpu        |
|LeNet_pytorch_gpu_scratch           | mnist        | pytorch:1.6.0-gpu          | 
|LeNet_tensorflow_gpu_scratch        | mnist        | tensorflow:1.14.0-gpu-py3  |
|Inceptionv3_tensorflow_gpu_scratch  | dog-vs-cat   | tensorflow:1.14.0-gpu-py3  |


## Preset engin docker images

* harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/tensorflow:2.3.0-gpu
* harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/pytorch:1.6.0-gpu
* harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/mxnet:2.0.0-gpu-py3
* harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/mindspore:1.1.1-gpu
* harbor.apulis.cn:8443/aiarts_v1.6.0_rc4_single/apulistech/tensorflow:1.15.0-gpu-py3

## Preset datasets

*Rerfer /data/dataset/storage/*

* mnist.tar.gz
* dog-vs-cat.tar.gz
* cifar-100-python.tar.gz
* fashion-mnist.git

**Tips** 

In fact, the commonly used datasets are relatively large， you can download it in your own environment by these links, in a relaxed time:

* coco datasets: https://cocodataset.org/
* hardhat : https://reboflow.com/ds
