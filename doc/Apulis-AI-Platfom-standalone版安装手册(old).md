## Apulis AI Platform Installer

### 一.部署整体流程介绍

- 从github下载安装脚本

- 修改安装程序配置文件

- 执行安装程序

- 部署完成登录平台

  ***注意：（1）服务器必须可以访问 Internet（2）服务器必须预先装好GPU/NPU驱动，如果是nvidia GPU卡需要装 nvidia-container-runtime*


### 二.部署执行链路

prepare fiel -> install docker -> install kubernetes -> node lable -> install nfs ->  install aiarts (apply.sh) -> preset_models-> deploy Prefabrication images

### 三.部署实操作

#### 1.从github 下载安装包

```shell
cd /home && git clone https://github.com/Asteven-zn/ApulisInstaller.git
```

- 安装包说明

  ```shell
  InstallApulis
  ├── applyrm.sh         AiArts_app删除脚本
  ├── apply.sh           AiArts_app部署脚本
  ├── calico.yaml        calico网络yaml
  ├── doc                文档
  ├── docker.sh          Docker部署
  ├── ingress.sh         平台访问入口配置脚本
  ├── install.sh         总安装脚本
  ├── k8s.sh             Kubernetes安装脚本
  ├── lab.sh             节点打label
  ├── LICENSE
  ├── menus.sql          menus菜单数据导库sql
  ├── mnist
  ├── model-gallery      
  ├── monitor.sh         Prometheus脚本
  ├── nfs.sh             nfs存储部署
  ├── preset_menus.sh    menus菜单数据导库脚本
  ├── preset_models.sh   导入预置镜像脚本
  ├── preset_models.sql  预置镜像sql
  ├── publicip.sh        配置公网及内网ip脚本
  ├── README.md
  ├── remove.sh          全平台删除脚本
  ├── ruser.sh           删除dlwsuser用户
  ├── userdlws.sh        创建dlwsuser用户
  └── yaml
  
  ```

#### 2.进入安装包目录

```shell
cd /home/InstallApulis
```

#### 3.执行install.sh 主安装脚本配置

bash install.sh，需要用户填写内部业务网IP，public-ip，domainame，输出如下：

```shell
root@apulis:~/InstallationYTung# bash install.sh
请输入业务网卡的ip地址:内部业务网IP
#-ip格式正确

您输入的业务网ip为：内部IP
Do you want to configure PublicNetworkIp? [yes/no] yes   （如果是云vm部署配置了public-ip选择yes）
Do you want to configure Domainame? [yes/no] yes         （如果是配置了domainame选择yes）

>#You deploy pattern：public network and domainame
请输入public-ip地址:public-ip
#-public-ip格式正确
请输入域名: domainame
```

#### 4.部署完成

讲到如下输出内容，平台部署完成

```shell
#>>>>>>-deploy finish-----------------------------------------------------------
#>>>>>>-Platform login information:
Public-ip:******   Private-address:******   Domainame:******
UserName:admin   Password:Wwjpfb

Platform data dir:/data/nfs/pvc
```

### 四.平台重置

#### 1.上层AiArts重置
- 执行applyrm.sh  删除kubernetes AiArts集群应用pod，此操作不删除数据
- 再执行apply.sh  重启AiArts集群应用pod
#### 2.全平台重置
- 执行remove.sh

*** 注意：全平台重置会将整个平台进行清除，请做好关键数据的备份*

*** 在执行remove.sh是会有 2个提示信息*

- 是否保留平台数据nfs存储（数据路径默认在/data/nfs/pvc）
- 是否备份PostgresSql数据库（安装目录下的postgres_backup.sql）

