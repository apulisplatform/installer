#!/bin/bash

##安装nvidia-container-runtime
#echo -e "\n-------------------------------nvidia-container-runtime********************************"
#curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | \
#  sudo apt-key add -
#distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
#curl -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list | \
#  sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
#sudo apt-get update
#apt-get install nvidia-container-runtime
#echo -e "\n如果 nvidia-container-runtime 已安装会输出以上内容，请忽略********************************"

#准备docker file
echo -e "\n\033[34m-------------------------------copy docker app-------------------------------------\033[0m"

docker_arr=(
"containerd"
"containerd-shim"
"ctr"
"docker"
"dockerd"
"docker-init"
"docker-proxy"
"runc"
)

if [[ ! -f "/usr/local/bin/docker" ]]
then
        mkdir -p /etc/docker && mkdir $HOME/.docker
        for item in ${docker_arr[*]}
        do	
        cp app/$item /usr/local/bin/
        done
fi


echo -e "\n\033[34m-------------------------------config docker daemon.json----------------------------\033[0m"

cp app/daemon.json /etc/docker/daemon.json

echo -e "\n\033[34m-------------------------------config docker.service--------------------------------\033[0m"

cat <<EOF>/etc/systemd/system/docker.service
[Unit]
Description=Docker Application Container Engine
Documentation=http://docs.docker.io

[Service]
Environment="PATH=/usr/local/bin:/bin:/sbin:/usr/bin:/usr/sbin"
ExecStart=/usr/local/bin/dockerd 
ExecStartPost=/sbin/iptables -I FORWARD -s 0.0.0.0/0 -j ACCEPT
ExecReload=/bin/kill -s HUP $MAINPID
Restart=always
RestartSec=5
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
Delegate=yes
KillMode=process

[Install]
WantedBy=multi-user.target
EOF

cat <<EOF>$HOME/.docker/config.json
{
        "experimental": "enabled"
}
EOF

echo -e "\n\033[34m-------------------------------start docker ----------------------------\033[0m"
systemctl daemon-reload && systemctl enable docker.service && systemctl restart docker.service