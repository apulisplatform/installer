#检查预准备环境

## clean 
cleaner(){
        echo -e "\033[31m Cleaning env, delete the installer ! \033[0m"
        cd .. && mv installer installer-bak-$(date "+%Y%m%d%H%M%S") && exit
}

## Assert GPU driver
assert_gpu(){
        lshw -numeric -C display | grep -i "nvidia" > /dev/null
        if [ $? -ne 0 ];then
                echo -e "\033[31m There is no nvidia GPU \033[0m"
        else
                gpu_info=`nvidia-smi 2>&1`
                if [ $? -ne 0 ];then
                        echo -e "\033[31m There is no gpu driver \033[31m"
                        # gpu_code=`lshw -numeric -C display | grep -i "nvidia" | grep "product" | awk '{print$NF}'`
                        # if [ $? -eq 0 ];then
                        #         echo -e "The gpu code is ${gou_code}"
                        # fi
                        cleaner
                else
                        echo -e "\033[32m GPU Driver is installed. \033[0m"     
                fi
        fi
        echo -e "-------------------------------check nvidia-container-runtime-------------------------------"
        dpkg --list | grep nvidia-container-runtime
        if [ $? -ne 0 ];then
                echo -e "\033[31m There is no nvidia-container-runtime.\033[0m"
                cleaner
        else
                echo -e "\033[32m Nvidia-container-runtime has been installed.\033[0m"
        fi
}
######################################################################################################
# Check steps
assert_gpu

