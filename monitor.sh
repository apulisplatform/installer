#部署custommetrics
echo -e "\n\033[34m#>>>>>>-running custommetrics service ------------------------------------\033[0m"

base_dir=`pwd`

custom=(
    "01.prometheus_operator.yaml"
    "02.prometheus_instance.yaml"
    "03.custom_metrics.yaml"
    "sample-metrics-app.yaml"
)

for customs in ${custom[*]}
do
    echo -e "\n\033[33m#>>>>>>-$base_dir/build/custommetrics/${customs}:first running status\033[0m" && kubectl apply -f $base_dir/build/custommetrics/$customs && sleep 3
done

for customs in ${custom[*]}
do
    echo -e "\n\033[33m#>>>>>>-$base_dir/build/custommetrics/${customs}:second running status\033[0m" && kubectl apply -f $base_dir/build/custommetrics/$customs && sleep 3
done

#部署prometheus监控###
echo -e "\n\033[34m#>>>>>>-running prometheus service ---------------------------------------\033[0m"
prom=(
    "01.node-exporter.yaml"
    "02.job-exporter.yaml"
    "03.watchdog.yaml"
    "04.prometheus-alerting.yaml"
    "05.prometheus.yaml"
    "06.grafana-ini.yaml"
    "07.grafana-config.yaml"
    "07.grafana-zh-config.yaml"
    "08.grafana.yaml"
    "08.grafana-zh.yaml"
    "09.alert-templates.yaml"
    "10.npu-exporter.yaml"
    #"alert-manager.yaml"
)

for proms in ${prom[*]}
do
    echo -e "\n\033[33m#>>>>>>-$base_dir/build/monitor/$proms/${proms}:running status\033[0m" && kubectl apply -f $base_dir/build/monitor/$proms && sleep 3
done

sleep 5
