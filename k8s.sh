#!/bin/bash
#获取服务器ip
host_ip=$1
export sys_arch=$sys_arch

#下载calico image
echo -e "\n\033[34m#>>>>>>-download calico image-------------------------------------------------\033[0m"

calico_image=(
  "harbor.apulis.cn:8443/aiarts_v1.5.0_rc8/apulistech/calico/node:v3.19.1"
  "harbor.apulis.cn:8443/aiarts_v1.5.0_rc8/apulistech/calico/pod2daemon-flexvol:v3.19.1"
  "harbor.apulis.cn:8443/aiarts_v1.5.0_rc8/apulistech/calico/cni:v3.19.1"
  "harbor.apulis.cn:8443/aiarts_v1.5.0_rc8/apulistech/calico/kube-controllers:v3.19.1"
)

for c_image in ${calico_image[*]}
  do
    docker pull $c_image
done

#下载k8s image
echo -e "\n\033[34m#>>>>>>-download kubernetes image-----------------------------------------\033[0m"

k8s_image=(
    "registry.aliyuncs.com/google_containers/kube-proxy:v1.18.0"
    "registry.aliyuncs.com/google_containers/kube-apiserver:v1.18.0"
    "registry.aliyuncs.com/google_containers/kube-controller-manager:v1.18.0"
    "registry.aliyuncs.com/google_containers/kube-scheduler:v1.18.0"
    "registry.aliyuncs.com/google_containers/pause:3.2"
    "registry.aliyuncs.com/google_containers/coredns:1.6.7"
    "registry.aliyuncs.com/google_containers/etcd:3.4.3-0"
)

for k_image in ${k8s_image[*]}
  do
    docker pull $k_image
done

#安装k8s
echo -e "\n\033[34m#>>>>>>-apt update and configure mirrors----------------------------------\033[0m"
apt-get update && apt-get install apt-transport-https -y

curl https://mirrors.aliyun.com/kubernetes/apt/doc/apt-key.gpg | apt-key add -

echo "deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list 

apt-get update

echo -e "\n\033[34m#>>>>>>-install kubelet=1.18.0-00 kubeadm=1.18.0-00 kubectl=1.18.0-00-----\033[0m"
apt-get install -y kubelet=1.18.0-00 kubeadm=1.18.0-00 kubectl=1.18.0-00


echo -e "\n\033[34m#>>>>>>-kubeadm init kubernetes-------------------------------------------\033[0m"
kubeadm init  --apiserver-advertise-address=$host_ip \
        --image-repository=registry.aliyuncs.com/google_containers \
        --kubernetes-version=1.18.0 \
        --control-plane-endpoint="$host_ip:6443" \
        --service-cidr=10.68.0.0/16 \
        --pod-network-cidr=172.20.0.0/16 \
        --service-dns-domain=cluster.local

echo -e "\n\033[34m#>>>>>>-configure .kube/config for user------------------------------------\033[0m"

if [ $HOME = "/root" ];then
        echo -e "#>>>>>>-create .kube_dir to root $HOME"
        mkdir -p $HOME/.kube
        
        echo -e "#>>>>>>-cp admin.conf to root in $HOME.kube/config"
        sudo cp -rf /etc/kubernetes/admin.conf $HOME/.kube/config
        
        echo -e "#>>>>>>-chown root.root to $HOME.kube/config"
        sudo chown $(id -u):$(id -g) $HOME/.kube/config
else
        user_name=$(echo $HOME | awk -F'/' '{print $NF}')
        echo -e "#>>>>>>-create .kube_dir to $user_name $HOME"
        su $user_name -c "mkdir -p $HOME/.kube"
        
        #echo -e "#>>>>>>-chown $user_name.$user_name to $HOME/.kube"
        #chown -R $user_name.$user_name $HOME/.kube

        echo -e "#>>>>>>-cp admin.conf to $user_name in $HOME.kube/config"
        cp -rf /etc/kubernetes/admin.conf $HOME/.kube/config

        echo -e "#>>>>>>-chown $user_name.$user_name to $HOME.kube/config"
        chown -R $user_name.$user_name $HOME/.kube/config
        
        #普通用户部署时root目录下也需创建config
        echo -e "#>>>>>>-configure to root kube/config"
        sudo mkdir -p /root/.kube
        sudo cp -rf /etc/kubernetes/admin.conf /root/.kube/config
        sudo chown $(id -u):$(id -g) /root/.kube/config
fi

sleep 10

echo -e "\n\033[34m#>>>>>>-kubeadm init complete---------------------------------------------\033[0m"

#安装calico 
echo -e "\n\033[34m#>>>>>>-install calico network--------------------------------------------\033[0m"
#kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
kubectl apply -f calico.yaml

if [ $? -ne 0 ];then
        echo -e "*************************install calico network failed********************************"
else    
        echo -e "*************************install calico network succeed********************************"
        kubectl get node
fi

sleep 3

#kubectl 命令自动补全
echo -e "\n\033[34m#>>>>>>-配置kubectl 命令自动补全--------------------------------------------------\033[0m"
kc=`grep "kubectl completion bash" ~/.bashrc`

if [ $? -ne 0 ];then
        apt install bash-completion
        echo "source /usr/share/bash-completion/bash_completion" >> ~/.bashrc
        echo "source <(kubectl completion bash)" >> ~/.bashrc
        source ~/.bashrc
	echo "#-succeed: The kubectl command completes the configuration automatically"
else
	echo "#-status: Kubectl command automatic completion has been configured"
fi

#节点打lable
echo -e "\n\033[34m#>>>>>>-node lable tag----------------------------------------------------\033[0m"
kubectl label node $HOSTNAME node-role.kubernetes.io/worker=worker
kubectl taint nodes --all node-role.kubernetes.io/master-
bash lab.sh

##添加/opt/kube/bin/kubectl
echo -e "\n\033[34m#>>>>>>-add /opt/kube/bin/kubectl-----------------------------------------\033[0m"
if [[ ! -f "/opt/kube/bin/kubectl" ]];then
    mkdir -p /opt/kube/bin
    cp /usr/bin/kubectl /opt/kube/bin/
fi
