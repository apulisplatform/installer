echo -e "\n-------------------------------------delete user: dlwsadmin----------------------------------------"
grep dlwsadmin /etc/passwd
if [ $? = 0 ]
then
    sed -i '/dlwsadmin/'d $HOME/.ssh/authorized_keys
    userdel -r dlwsadmin
    rm -rf /home/dlwsadmin
else
    echo "There is no user named dlwsadmin"
fi
