#!/bin/bash

## 判断系统架构:aarch64 or x86_64
sys_arch=$(uname -m)
echo -e "\033[34mSystem architecture is $sys_arch\033[0m"
export sys_arch=$sys_arch

kubectl get node --show-labels

x86_64_lab=(
    "kubernetes.io/role=master"
    "FragmentGPUJob=active"
    "aiarts-backend=active"
    "aiarts-frontend=active"
    "alert-manager=active"
    "archType=amd64"
    "dataset-manager=active"
    "gpu=active"
    "npu=active"
    "gpuType=nvidia_gpu_amd64"
    "grafana=active"
    "image-label=active"
    "jobmanager=active"
    "mlflow=active"
    "nginx=active"
    "postgres=active"
    "prometheus=active"
    "restfulapi=active"
    "watchdog=active"
    "webportal=active"
    "webui=active"
    "worker=active"
);

arm64_lab=(
    "kubernetes.io/role=master"
    "FragmentGPUJob=active"
    "aiarts-backend=active"
    "aiarts-frontend=active"
    "alert-manager=active"
    "archType=arm64"
    "dataset-manager=active"
    "gpu=active"
    "npu=active"
    "gpuType=huawei_npu_arm64"
    "grafana=active"
    "image-label=active"
    "jobmanager=active"
    "mlflow=active"
    "nginx=active"
    "postgres=active"
    "prometheus=active"
    "restfulapi=active"
    "watchdog=active"
    "webportal=active"
    "webui=active"
    "worker=active"
    "series="
);

if [[ $sys_arch = "x86_64" ]];then
    length=${#x86_64_lab}
    for item in ${x86_64_lab[*]}
    do
            kubectl label nodes $HOSTNAME $item --overwrite
    done
else [[ $sys_arch = "aarch64" ]]
    length=${#arm64_lab}
    for item in ${arm64_lab[*]}
    do
            kubectl label nodes $HOSTNAME $item --overwrite
    done
fi

kubectl get node --show-labels