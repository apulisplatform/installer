#!/bin/bash

#install.sh中设置开关来判断平台访问的方式

ip_dir=`pwd`

restfulapi2_file=(
    "01.restful-cm.yaml"
    "config.yaml"
)

webui3_file=(
    "01.webui-cm.yaml"
    "local.yaml"
)

#修改环境内网ip
echo -e "\n\033[34m#>>>>>>-configure restful and webui service access Ip Address----------\033[0m"

if [[ $public_stat == 1 ]];then
        for file in ${restfulapi2_file[*]}
        do
            sed -i s/exip/$input_domain/g $ip_dir/build/restfulapi2/$file
        done

        for file in ${webui3_file[*]}
        do
            sed -i s/exip/$eth_ip/g $ip_dir/build/webui3/$file
        done
        
elif [[ $public_stat == 2 ]];then
        for file in ${restfulapi2_file[*]}
        do
            sed -i s/exip/$exeth_ip/g $ip_dir/build/restfulapi2/$file
        done

        for file in ${webui3_file[*]}
        do
            sed -i s/exip/$eth_ip/g $ip_dir/build/webui3/$file
        done

elif [[ $public_stat == 3 ]];then
        for file in ${restfulapi2_file[*]}
        do
            sed -i s/exip/$input_domain/g $ip_dir/build/restfulapi2/$file
        done

        for file in ${webui3_file[*]}
        do
            sed -i s/exip/$eth_ip/g $ip_dir/build/webui3/$file
        done

elif [[ $public_stat == 4 ]];then
        for file in ${restfulapi2_file[*]}
        do
            sed -i s/exip/$eth_ip/g $ip_dir/build/restfulapi2/$file
        done

        for file in ${webui3_file[*]}
        do
            sed -i s/exip/$eth_ip/g $ip_dir/build/webui3/$file
        done
        
else 
        echo "public ip config faild"
fi
