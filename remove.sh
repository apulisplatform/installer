#!/bin/bash
install_dir=$(pwd)
sys_arch=$(uname -m)

read -r -p "Do you want to delete nfsservice and nfsdata? [yes/no] " input_delenfs
read -r -p "Do you want to backup postgresql data? [yes/no] " input_baksql

####################################### function #############################################

backupdata_function(){
    echo -e "\n---------------------------backup postgresql data-------------------------"
    dockerid=$(docker ps | grep 'k8s_postgres_postgres' | awk '{print $1 }' | awk 'NR=1' | head -1)
    docker exec -it $dockerid /bin/bash -c "pg_dumpall -U postgres -h localhost >/home/postgres_backup.sql"
    docker cp $dockerid:/home/postgres_backup.sql ./
}

delete_nfsdata_function(){
    echo -e "\n-------------------------------delete nfsdata------------------------------"

    systemctl stop nfs-kernel-server
    systemctl disable nfs-kernel-server
    umount /mntdlws
    systemctl stop rpcbind
    systemctl disable rpcbind

    rm -rf /etc/exports
    rm -rf /data/nfs
    rm -rf /mntdlws

    echo -e "\n--------------------------delete nfsdata finish----------------------------"
}


#配置menus
menus_function(){
    echo -e "\n\033[34m#>>>>>>-change menus data status--------------------------------------------------\033[0m"
    menus_data=`grep status ./preset_menus.sh`
    if [[ $menus_data = "status=1" ]];then
	sed -i s/status=1/status=0/g ./preset_menus.sh
        echo -e "change menus data status is status=0"
    else
        echo -e "The menus data is empty and status is status=0"
    fi
}
#配置models
models_frunciton(){
    echo -e "\n\033[34m#>>>>>>-change models data status-------------------------------------------------\033[0m"
    models_data=`grep status ./preset_models.sh`
    if [[ $models_data = "status=1" ]];then
        sed -i s/status=1/status=0/g ./preset_models.sh
	echo -e "change models data status is status=0"
    else
        echo -e "The models data is empty and status is status=0"
    fi
}
#配置deploy
deploy_function(){
    echo -e "\n\033[34m#>>>>>>-change deploy data status-------------------------------------------------\033[0m"
    deploy_data=`grep status ./model-gallery/deploy.sh`
    if [[ $deploy_data = "status=1" ]];then
	sed -i s/status=1/status=0/g ./model-gallery/deploy.sh
        echo -e "change deploy data status is status=0"
    else
        echo -e "The deploy data is empty and status is status=0"
    fi
}

####################################### program ##############################################
#### 是否备份数据库
if [[ "$input_baksql" =~ ^[Yy][Ee][Ss]$ ]];then
    backupdata_function
    backupfile=`ls $(pwd) | grep postgres_backup.sql`
    echo -e "postgresql-backupdata: $(pwd)/$backupfile"
    sleep 3
else [[ "$input_baksql" =~ ^[Nn][Oo]$ ]]
    echo "postgresql will be delete!!!"
fi

echo -e "\n-------------------------------uninstall Apulis AI Platform----------------------------"
bash applyrm.sh

rm -rf build
rm -rf app

echo -e "\n---------------------------------uninstall kubernetes----------------------------------"
kubeadm reset -f

rm -rf /etc/nginx
rm -rf /opt/kube
rm -rf /etc/kubernetes

systemctl stop kubelet

rm -rf $HOME/.kube
rm -rf /root/.kube
rm -rf /var/lib/etcd

#### 是否删除nfs及data
if [[ "$input_delenfs" =~ ^[Yy][Ee][Ss]$ ]];then
    echo -e "\n-----------------------------start delete nfs data----------------------------------"
    delete_nfsdata_function
    menus_function
    models_frunciton
    deploy_function
    rm -rf mnist
    rm -rf model-gallery
    rm -rf mnist.tar.gz
    rm -rf model-gallery.tar.gz
    #如果删除nfsdata，要保留credentials下次部署密码不变
    cp -r credentials credentials.bak

else [[ "$input_delenfs" =~ ^[Nn][Oo]$ ]]
    echo -e "\n-----------------------------Not delete nfs data!!!---------------------------------"
    #如果不删除nfsdata，要保留credentials下次部署密码不变
    cp -r credentials credentials-$(date "+%Y%m%d%H%M%S").bak
    #需要将数据库数据导入状态改为1，避免重复导入
    sed -i "s/status=0/status=1/g" preset_menus.sh
    sed -i "s/status=0/status=1/g" preset_models.sh
    sed -i "s/status=0/status=1/g" ./model-gallery/deploy.sh
fi

echo -e "\n-----------------------------------uninstall docker------------------------------------"
systemctl disable docker.service && systemctl stop docker.service && systemctl daemon-reload

echo -e "\n-------------------------------------delete docker file----------------------------------------"

docker_arr=(
  "containerd"
  "containerd-shim"
  "ctr"
  "docker"
  "dockerd"
  "docker-init"
  "docker-proxy"
  "runc"

)

if [[ -f "/usr/local/bin/docker" ]]
  then
    
    for item in ${docker_arr[*]}
    do
        rm -rf /usr/local/bin/$item
    done

fi

rm -rf /etc/docker
rm -rf $HOME/.docker
rm -rf /etc/systemd/system/docker.service
rm -rf app.tar.gz
rm -rf app_$sys_arch.tar.gz

echo -e "\n-------------------------------------delete user: dlwsadmin----------------------------------------"
grep dlwsadmin /etc/passwd
if [ $? = 0 ]
then
    sed -i '/dlwsadmin/'d $HOME/.ssh/authorized_keys
    userdel -r dlwsadmin
    rm -rf /home/dlwsadmin
else
    echo "There is no user named dlwsadmin"
fi

if [ $? = 0 ]
then
    echo -e "\n#>>>>>>-satus: uninstall succeed"
else
    echo -e "\n#>>>>>>-satus: uninstall fail"
fi
