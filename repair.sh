################################################## 数据恢复 ###########################################################################
#!/bin/bash
#define vars
postgre_name=$(kubectl get pod -A | grep postgres | awk -F " +" '{print $2}')
new_clusterid=$(cat credentials/cluster-id)
#alldlwdb_name=$(kubectl exec -i -n kube-system $postgre_name -- psql -U postgres -c "\l" | grep DLW | awk -F " +" '{print $2}' | grep -v "${new_clusterid}")
olddlwdb_name=$(kubectl exec -i -n kube-system $postgre_name -- psql -U postgres -c "\l" | grep DLW | awk -F " +" '{print $2}' | grep -v "${new_clusterid}")
new_postgrespwd=$(cat credentials/postgres.pwd)
dockerid=$(docker ps | grep 'k8s_postgres_postgres' | awk '{print $1 }' | awk 'NR=1' | head -1)
base_dir=$(pwd)

kubectl exec -i -n kube-system $postgre_name -- psql -U postgres -d user_group -c "delete from public.user where user_name = 'admin';"

##repair postgress password
kubectl exec -i -n kube-system $postgre_name -- psql -U postgres -c "ALTER USER postgres WITH PASSWORD '$new_postgrespwd';"
#
##restart pod
#fail_pod=$(kubectl get pod -A | egrep -v '(NAME|Running|Completed)' | awk -F " +" '{print $2}')
#for restart_pod in ${fail_pod}
#do
#        kubectl delete pod
#done

#backup DLWCluster Old-DataBase data
for backupdb in ${olddlwdb_name}
do
        docker exec -i $dockerid /bin/bash -c "pg_dump -U postgres $backupdb -h localhost >/home/DLW_backup_$backupdb.sql"
        docker cp $dockerid:/home/DLW_backup_$backupdb.sql ./
done

#stop service pod
stoppod_function(){
        echo -e "\n\033[34m#>>>>>>-uninstall monitor and custommetrics -----------------------------------------\033[0m"
        kubectl delete -f $base_dir/build/monitor/.
        kubectl delete -f $base_dir/build/custommetrics/.

        echo -e "\n\033[34m#>>>>>>-uninstall knative and cvat -----------------------------------------\033[0m"
        arr1=(
            "cvat-model"
            "cvat"
        #    "kfserving"
            "knative"
        )

        for item1 in ${arr1[*]}
        do
            #echo $item
            n=`cd $base_dir/build/$item1 && ls | grep '^[0-9]'`
            #echo $n
            cd $base_dir/build/$item1 \
            && for file in $n
                do
                echo -e "\n\033[33m#>>>>>>-$base_dir/build/$item1/${file}:running status\033[0m"; kubectl delete -f $file
            done
            cd $base_dir
        done

        echo -e "\n-------------------------------uninstall istio service------------------------------------------"
        cd $base_dir/build/ \
        && /opt/kube/bin/istioctl manifest generate --set values.global.jwtPolicy=first-party-jwt | kubectl delete -f - \
        && cd $base_dir

        sleep 3

        echo -e "\n-------------------------------uninstall Apulis AI Platform service ----------------------------"

        arr2=(
            "model-manager"
        #    "data-platform"
        #    "image-label"
            "volcanosh"
        #    "mlflow"
            "aiarts-frontend"
            "aiarts-backend"
            "webui3"
            "openresty"
            "nginx"
        #    "custommetrics"
            "jobmanager2"
            "custom-user-dashboard"
            "restfulapi2"
        #    "postgres"
            "nvidia-device-plugin"
        )

        # for 遍历服务目录
        for item2 in ${arr2[*]}
        do
            #echo $item
            n=`cd $base_dir/build/$item2 && ls | grep '^[0-9]'`
            #echo $n
            cd $base_dir/build/$item2 \
            && for file in $n
                do
                echo -e "\n\033[33m#>>>>>>-$base_dir/build/$item1/${file}:running status\033[0m"; kubectl delete -f $file
            done
            cd $base_dir
        done
}

stoppod_function

#delete new DLWCluster DB
kubectl exec -i -n kube-system $postgre_name -- psql -U postgres -c "DROP DATABASE \"DLWCluster-${new_clusterid}\";"

#create new DLWCluster DB
kubectl exec -i -n kube-system $postgre_name -- psql -U postgres -c "CREATE DATABASE \"DLWSCluster-${new_clusterid}\""

#recovery DLWCluster DB data
docker cp ./DLW_backup_* $dockerid:/home/
docker exec -i $dockerid /bin/bash -c "psql -U postgres -d \"DLWSCluster-${new_clusterid}\" -f /home/DLW_backup_*" > /dev/null 2>&1

bash apply.sh
