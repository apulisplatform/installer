#!/bin/bash
# Editor Thomas
# Update 2021-06-27
# License MIT

# Display information

echo """

##########################################################################################################
## Ha ha ha, Thank you for startup Apulis Installer (Beta)!                                             ##
## Before starting the installation, You must make sure the system is newly installed !                 ##
## and There are no important personal files that need to keep, We can't guarantee that                 ##
## no documents will be destroyed. and sincerely apologize for the inconvenience.                       ##
## Matainer: thomas, ning, bing                                                                         ##
## License: MIT                                                                                          ##
## Supported By                                                                                         ##
##                                                                                                      ##
##      _                _ _       ___                                                                  ##
##     / \   _ __  _   _| (_)___  |_ _|_ __   ___                                                       ##
##    / _ \ | '_ \| | | | | / __|  | || '_ \ / __|                                                      ##
##   / ___ \| |_) | |_| | | \__ \  | || | | | (__ _                                                     ##
##  /_/   \_\ .__/ \__,_|_|_|___/ |___|_| |_|\___(_)                                                    ##
##          |_|                                                                                         ##
##                                                                                                      ##
## Apulis AI Platform Aims To Provide An E2E DL&ML Platform                                             ##
##########################################################################################################

"""
for i in `seq -w 15 -1 0`
do
    echo -en "We will start the installation in \e[0;31m$i\e[0m seconds ...\r"  
  sleep 1;
done
echo """ 
>>>>>>Starup Date: $(date) 
Once you executing the script, If not necessary, please do not interrupt ! 
=========================================================================================================
"""
# Take a moment 
sleep 3s

git config core.filemode false
git config --global core.fileMode false
apt install -y neofetch 
echo -e "\033[32m>>>>>>-The host basement info: \033[0m"
neofetch --gpu_brand on --gpu_type dedicated --off
# Get GPU info and install driver plugin
curl https://get.docker.com | sh \
  && sudo systemctl --now enable docker
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
  && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - \
  && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
curl -s -L https://nvidia.github.io/nvidia-container-runtime/experimental/$distribution/nvidia-container-runtime.list | sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
apt update 
apt install -y nvidia-docker2 nvidia-container-runtime  gawk jq\
  && systemctl restart docker  \
  && apt clean   \
  && apt autoclean   \
  && apt -y autoremove 
CUDA=$(nvidia-smi | awk -F"CUDA Version:" 'NR==3{split($2,a," ");print a[1]}');
docker run --rm --gpus all nvidia/cuda:$CUDA-base nvidia-smi
