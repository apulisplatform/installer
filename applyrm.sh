#!/bin/bash

base_dir=`pwd`

echo -e "\n\033[34m#>>>>>>-uninstall monitor and custommetrics -----------------------------------------\033[0m"
kubectl delete -f $base_dir/build/monitor/.
kubectl delete -f $base_dir/build/custommetrics/.

echo -e "\n\033[34m#>>>>>>-uninstall knative and cvat -----------------------------------------\033[0m"
arr1=(
    "cvat-model"
    "cvat"
#    "kfserving"
    "knative"
)

for item1 in ${arr1[*]}
do
	#echo $item
	n=`cd $base_dir/build/$item1 && ls | grep '^[0-9]'`
	#echo $n
	cd $base_dir/build/$item1 \
	&& for file in $n
        do
	    echo -e "\n\033[33m#>>>>>>-$base_dir/build/$item1/${file}:running status\033[0m"; kubectl delete -f $file
	done
	cd $base_dir
done

echo -e "\n-------------------------------uninstall istio service------------------------------------------"
cd $base_dir/build/ \
   && /opt/kube/bin/istioctl manifest generate --set values.global.jwtPolicy=first-party-jwt | kubectl delete -f - \
   && cd $base_dir

sleep 3

echo -e "\n-------------------------------uninstall Apulis AI Platform service ----------------------------"

arr2=(
    "model-manager"
#    "data-platform"
#    "image-label"
    "volcanosh"
#    "mlflow"
    "aiarts-frontend"
    "aiarts-backend"
    "webui3"
    "openresty"
    "nginx"
#    "custommetrics"
    "jobmanager2"
    "custom-user-dashboard"
    "restfulapi2"
    "postgres"
    "nvidia-device-plugin"
)

# for 遍历服务目录
for item2 in ${arr2[*]}
do
	#echo $item
	n=`cd $base_dir/build/$item2 && ls | grep '^[0-9]'`
	#echo $n
	cd $base_dir/build/$item2 \
	&& for file in $n
        do
	    echo -e "\n\033[33m#>>>>>>-$base_dir/build/$item1/${file}:running status\033[0m"; kubectl delete -f $file
	done
	cd $base_dir
done


kubectl delete -f $base_dir/build/storage-nfs/.

if [ $? -ne 0 ];then
    echo -e "\n#>>>>>>-failed: Apulis AI Platform UnInstaller failed"
else    
    echo -e "\n#>>>>>>-succeed: Apulis AI Platform UnInstaller succeed"
fi
