# Apulis AI Platform Installer

## 适用环境

* 集群规模： 一体机（单机）
* 系统版本： Ubuntu Server 18.04/20.04 x86-64 （arm64 开发中）
* 算力：     NVIDIA GPU  （Huawei NPU  开发中）
* 浏览器：   Chrome latest version
* 网络：     服务器必须可以访问 Internet，下载安装资源。
* 帐号权限： 安装时必须使用root帐号登陆平台

## 1. 部署整体流程介绍

- 安装主机的操作系统
- 安装GPU驱动
- 执行安装指令

- 确认网络IP或域名

- 查看安装登陆信息，并在chrome中登陆平台

- 管理员初始化平台配置

***Tips*** 在安装执行结束前，请不要关闭终端或休眠主机。    
 
## 2. 执行安装指令

* 手动安装GPU驱动

  **Tips** 如果使用的是阿里云，AWS，Azure 等云服务器，GPU驱动可能已经安装好，只需检查或升级驱动版本。

  + 查询GPU型号  

    `apt install -y neofetch && neofetch --gpu_brand on --gpu_type dedicated --off`

  + 从[NVIDIA官网](https://www.nvidia.com/Download/driverResults.aspx/146678/en-us)下载相应的驱动到服务器本地

    > i) `dpkg -i nvidia-diag-driver-local-repo-ubuntu1804-410.129_1.0-1_amd64.deb’ for Ubuntu
    > 
    > ii) `apt-get update`
    > 
    > iii) `apt-get install cuda-drivers`
    > 
    > iv) `reboot`
``
  + 检查驱动是否安装好

    `nvidia-smi`

* 使用root执行安装指令
  
  *安装日志默认放在 `/var/log/apulis_installation-[DATAE].log`*

  ```bash
  sudo apt install -y git unzip tar wget curl && sudo git clone -b develop https://gitee.com/apulisplatform/installer.git && cd installer && bash install.sh | tee /var/log/apulis_installation-$(date "+%Y%m%d%H%M%S").log
  ```

### 2.1. 确认业务网络、外网可访问IP或域名

  ```shell
  Please enter IP address for the platform LAN service:
  The service IP you entered is： [LAN IP]
  Do you want to configure Public Ip? [yes/no] yes                                                                     #（如果是云vm部署配置了public-ip选择yes）
  Do you want to configure Domainame? [yes/no] yes                                                                     #（如果是配置了domainame选择yes）
  Please enter the IP for public network access. If you don\'t have public IP, please use LAN IP instead: [PUBLIC IP]  # 默认作为平台访问IP
  Please enter domainname: [DOMAINNAME]
  ```

### 2.2. 查看部署完成的平台登陆信息

  看到如下输出内容，说明平台已基本安装完成，管理员可以登陆平台查看了！

  ```shell
  #>>>>>>-The platform has been successfully deployed !-----------------------------------------------------------
  #>>>>>>-Platform login information:
  Public-ip:******   Private-address:******   Domainame:******
  UserName:admin   Password:Wwjpfb

  Platform data dir:/data/nfs/pvc
  ```

## 3. 预置镜像和数据集

  平台默认会预置一些常用的模型，数据集和docker镜像，但由于文件比较大，下载时间长容易中断，可能使得在使用平台的时候出现一些**文件找不到**或**运行失败**或**任务挂起**等常见问题；
您可以参考我们的预置数据列表，自行下载或配置相关文件，也可以直接联系我们给您同步数据或文件。

[预置数据列表](doc/Preset-models-info.md)

*Rerfer /data/model-gallery/models*

|model templates                     |datasets      |engin image                 |
|------------------------------------|--------------|----------------------------|
|ResNet50_mindspore_gpu              | dog-vs-cat   | mindspore:1.1.1-gpu        |
|LeNet_pytorch_gpu_scratch           | mnist        | pytorch:1.6.0-gpu          | 
|LeNet_tensorflow_gpu_scratch        | fashion-mnist        | tensorflow:1.14.0-gpu-py3  |
|Inceptionv3_tensorflow_gpu_scratch  | dog-vs-cat   | tensorflow:1.14.0-gpu-py3  |

## 4. 管理员初始化平台

请参考[快速使用指引](doc/快速使用指引.md), 如有疑问或异常问题可以直接联系我们，提交问题到[Issues](https://gitee.com/apulisplatform/installer/issues)


## 5. 维护平台环境

### 5.1. 重置平台

- 先执行 applyrm.sh  删除集群应用pod，此操作不删除数据.
- 再执行 apply.sh  重启集群应用pod.

### 5.2. 重置集群
  
  **警告 !** 基础虚拟环境，数据库，存储服务器，镜像服务等会被全部重置，务必提前备份密钥证书和数据，否则数据可能会丢失！

  `bash remove.sh`

  **注意：集群重置会将整个环境清除，请做好关键数据的备份, 在执行remove.sh是会提示**

  - 是否保留平台数据（默认在/data/nfs/pvc）
  - 是否备份数据库（默认在安装目录下的postgres_backup.sql）

## 模型训练或推理注意事项

1. 平台已经预置了模型和数据，用户可以根据自己的需要更新

  + 常用的模型：LeNet, inceptionv3, SSD，MobileNetv2, FasterRCNN, YOLOv3...
  + 镜像: tensorflow:1.15/2.3.0, mindspore:1.1.1, mxnet:2.0.0, pytorch:1.6.0...
  + 数据集： fashion-mnist, cifar10, dog-vs-cat...

2. 实际环境的GPU驱动，cuda版本，框架版本都支持使用者根据自己的需要升级更新

3. 平台支持上传coco数据集，其他voc等标注格式也训练和测试，推荐tar，tar.gz格式。

4. 我们提供的环境以python3为主，推荐使用python3.7+

5. 华为 Model Zoo, 或其他社区（tensorflow, pytorch, paddle）上下载的模型, 需要参考平台环境参数和算力做一定的适配。

## 联系我们

我们非常欢迎大家，试用平台，共同探讨AI世界，思考后AI时代的到来！

* 如果您在安装使用中需要任何问题都可以在 [平台主页](https://gitee.com/apulisplatform/apulis_platform)联系我们。
* 希望您可以将任何错误或日志提交到[Issues](https://gitee.com/apulisplatform/installer/issues),我们会尽快定位答复。
* 如果您的环境或设备可以远程连接，我们可以第一时间远程协助您处理疑难问题。

## 如果是通过远程SSH终端执行安装出现超时中断，可以尝试以下方法修复安装

1. 可以尝试执行安装目录下的`bash retry_preset.sh | tee /var/log/retry-timeout-$(date "+%Y%m%d%H%M%S").log`脚本重新配置资源。

2. 清理安装环境后，重新使用screen重新执行安装指令。

2.1. 执行安装目录下的 `bash remove.sh`, 不备份任何文件; 清理安装环境；

2.2. 再在安装目录下执行 `cd .. && mv installer installer-back`，将已经下载的安装包备份或移除；

2.3. 重新执行以下安装指令。

```
# 使用screen执行安装指令
sudo apt update && apt install -y screen git  \
    && screen_name=$"my_screen"  \
    && screen -dmS $screen_name   \
    && screen -x -S $screen_name -p 0 -X stuff "sudo git clone -b develop https://gitee.com/apulisplatform/installer.git && cd installer && bash install.sh | tee /var/log/apulis_installation-$(date "+%Y%m%d%H%M%S").log"  \
    && screen -x -S $screen_name -p 0 -X stuff $'\n'  \
    && screen -r $screen_name

# 退出 screen
screen -x -S $screen_name -p 0 -X stuff "exit"  
screen -x -S $screen_name -p 0 -X stuff "\n"
```


